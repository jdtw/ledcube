/*
 * allon v0.1
 * Copyright 2012 John Wood
 *
 * Turns all of the leds in the cube on one by one
 *
 */

// You need to include the SPI header because
// we are using SPI to talk to the MAX6971. 
#include <SPI.h>
#include <LEDCube.h>

void setup()
{
    int anodes[4] = {5, 6, 7, 8};
    Cube.initialize(9, anodes);
    Cube.startRefresh(1); // Refresh rate of 1ms.
}

void loop()
{
    for (int x=0; x<4; ++x)
    {
        for (int y=0; y<4; ++y)
        {
            for (int z=0; z<4; ++z)
            {
                Cube.ledOn(x, y, 0);
                delay(100);
            }
        }
    }

    Cube.clear();
}
