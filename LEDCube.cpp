/*
 * LEDCube.cpp
 * Version 0.01 Jan 2012
 *
 * Copyright 2012 John Wood
 * Licensed under the MIT License 
 * http://www.opensource.org/licenses/MIT
 *
 */

#include <LEDCube.h>

LEDCube Cube;

LEDCube::LEDCube() :
    _ticks(0),
    _tcnt2(0),
    _refreshRate(0),
    _latch(0),
    _layer(0)
{
    memset((void*)_anodes, 0, CUBE_SIZE*_anodes[0]);
    clear();
}

void LEDCube::initialize(int latch, int anodes[CUBE_SIZE])
{
    _latch = latch;

    for (int i=0; i<CUBE_SIZE; ++i)
    {
        _anodes[i] = anodes[i];
        pinMode(_anodes[i], OUTPUT);
        digitalWrite(_anodes[i], LOW);
    }

    SPI.begin();
    digitalWrite(10, LOW); // SS low
}

void LEDCube::startRefresh(unsigned long ms)
{
    // Disable all timer2 interrupts including
    // the overflow interrupt while we set up.
    cbi(TIMSK2, TOIE2); 
    cbi(TIMSK2, OCIE2A);
    cbi(TIMSK2, OCIE2B);

    // Use the internal timer. We have to do this
    // before puting the timer into Normal mode
    // because the Atmega328 datasheet says that
    // changing this may corrupt the TCCR2A and
    // TCCR2B registers.
    cbi(ASSR, AS2);

    // Put Timer2 into Normal mode.
    cbi(TCCR2A, WGM20);
    cbi(TCCR2A, WGM21);
    cbi(TCCR2B, WGM22);

    // Set prescalar to 128.
    sbi(TCCR2B, CS20);
    cbi(TCCR2B, CS21);
    sbi(TCCR2B, CS22);

    // 16000000Hz / 128 = 125000Hz = 8us
    // 1000us / 8us = 125
    // 256 - 125 = 131
    TCNT2 = _tcnt2 = 131;

    // Set the counters we will be using.
    _refreshRate = ms ? ms : 1;
    _ticks = 0;

    // Done with setup. Enable the interrupt.
    sbi(TIMSK2, TOIE2);
}


// clear() only works if refresh is running.
void LEDCube::clear()
{
    memset((void*)_leds, 0, CUBE_SIZE*CUBE_SIZE*CUBE_SIZE);
}

void LEDCube::ledOn(int x, int y, int z)
{
    _leds[x][y][z] = 1;
}

void LEDCube::ledOff(int x, int y, int z)
{
    _leds[x][y][z] = 0;
}

void LEDCube::toggleLed(int x, int y, int z)
{
    _leds[x][y][z] ^= 1; 
}

unsigned char LEDCube::getState(int x, int y, int z)
{
    return _leds[x][y][z];
}

void LEDCube::_refresh()
{
    TCNT2 = _tcnt2;

    if (++_ticks == _refreshRate)
    {
        digitalWrite(_anodes[_layer], LOW);
        _layer = (_layer+1) % CUBE_SIZE;
        digitalWrite(_latch, LOW);

        unsigned char data = 0;

        // Note: we are assuming that the number of
        // cathodes is divisible by 8. If it is not,
        // 'bit' will have to be initialized to an 
        // offset.
        int bit = 7;

        for (int x = CUBE_SIZE-1; x >= 0; --x)
        {
            for (int y = CUBE_SIZE-1; y >= 0; --y)
            {
                if (_leds[x][y][_layer])
                {
                    sbi(data, bit);
                }

                if (bit-- == 0)
                {
                    SPI.transfer(data);
                    data = 0;
                    bit = 7;
                }
            }
        }

        digitalWrite(_latch, HIGH);
        digitalWrite(_anodes[_layer], HIGH);
        _ticks = 0;
    }
}

ISR(TIMER2_OVF_vect)
{
    Cube._refresh();
}
