/*
 * LEDCube.h
 * Version 0.01 Jan 2012
 *
 * Copyright 2012 John Wood
 * Licensed under the MIT License 
 * http://www.opensource.org/licenses/MIT
 */

#ifndef ledcube_h
#define ledcube_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#include <pins_arduino.h>
#endif

#include <SPI.h>

#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif 

#define CUBE_SIZE 4

class LEDCube
{
public:
    LEDCube();
    void startRefresh(unsigned long ms);
    void initialize(int latch, int anodes[CUBE_SIZE]);

    void clear();
    void ledOn(int x, int y, int z);
    void ledOff(int x, int y, int z);
    void toggleLed(int x, int y, int z);
    unsigned char getState(int x, int y, int z);

    // This can't be private, but it should be.
    void _refresh();

private:
    // pins
    int _anodes[CUBE_SIZE];
    int _latch;

    // LED abstraction
    volatile unsigned char _leds[CUBE_SIZE][CUBE_SIZE][CUBE_SIZE];

    // timer/ISR 
    volatile unsigned long _ticks;
    unsigned int _tcnt2;
    unsigned long _refreshRate;
    volatile unsigned char _layer;
};

extern LEDCube Cube;
#endif // ledcube_h
